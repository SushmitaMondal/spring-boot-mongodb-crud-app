package com.spring.example.springbootmongoDBexample;

import com.spring.example.springbootmongoDBexample.document.Users;
import com.spring.example.springbootmongoDBexample.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(
//		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
//		classes = SpringBootMongoDbExampleApplication.class
)
////@AutoConfigureMockMvc
////@TestPropertySource(locations = "./resources/application-test.properties")
//
////@ContextConfiguration(locations = "classpath:pom.xml")
//@TestPropertySource(locations = "classpath:application-test.properties")
public class SpringBootMongoDbExampleApplicationTests {

//	private Users anUser;
//	@Autowired
//	UserService anUserService;
//
////	@Autowired
////	MockMvc mockMvc;

	@Test
	public void contextLoads() {
	}

//	@Test
//	public void addUser(){
//		anUser = new Users(1,"Test", "Testing Designation","Testing Team");
//
//		anUserService.insert(anUser);
//		Optional<Users> resultUsers = anUserService.getUser(1);
//		if(resultUsers.isPresent()){
//			Users resultUser = resultUsers.get();
//			Assert.assertEquals(anUser, resultUser);
//		}
//	}

//	private void returnPath() throws IOException {
//		final PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
//		ppc.setLocation(
//			new PathMatchingResourcePatternResolver().getResources("classpath*:application-test.properties")
//		);
//
////		return path;
//	}
}
