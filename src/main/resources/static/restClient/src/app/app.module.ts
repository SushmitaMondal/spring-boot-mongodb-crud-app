import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";


import { AppComponent } from './app.component';
import { ListuserComponent } from './components/listuser/listuser.component';
import { UserformComponent } from './components/userform/userform.component';
import { SearchuserComponent } from './components/searchuser/searchuser.component';
import {UserService} from "./shared-service/user.service";

const appRoutes:Routes =[
  {path: '', component:ListuserComponent},
  {path:'create', component:UserformComponent},
  {path:'search', component:SearchuserComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    ListuserComponent,
    UserformComponent,
    SearchuserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
