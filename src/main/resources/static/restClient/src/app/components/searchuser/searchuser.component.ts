import { Component, OnInit } from '@angular/core';
import {UserService} from "../../shared-service/user.service";
import {User} from "../../user";
import {Router} from "@angular/router";


@Component({
  selector: 'app-searchuser',
  templateUrl: './searchuser.component.html',
  styleUrls: ['./searchuser.component.css']
})
export class SearchuserComponent implements OnInit {

  private users:User[];
  private user:User;
  private id:Number;
  constructor(private userService:UserService, private router:Router) { }

  ngOnInit() {
    console.log(this.userService.getID());
    this.userService.getUser(this.userService.getID()).subscribe((users)=>{
      console.log(users);
      if(users == null){
        this.router.navigate(['/']);
      }
      this.user = users;
    },(error)=>{
      console.log(error);
    })
    // this.user = this.userService.getter();
  }

  deleteUser(user){
    this.userService.deleteUser(user.id).subscribe((data) =>{
      this.users.splice(this.users.indexOf(user),1);
      this.router.navigate(['/']);
    },(error)=>{
      console.log(error);
    });
  }

  updateUser(user){
    this.userService.setter(user);
    this.router.navigate(['/create']);
  }

  goBack(){
    this.router.navigate(['/']);
  }

}
