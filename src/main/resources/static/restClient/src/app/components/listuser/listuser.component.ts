import { Component, OnInit } from '@angular/core';
import {UserService} from "../../shared-service/user.service";
import {User} from "../../user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-listuser',
  templateUrl: './listuser.component.html',
  styleUrls: ['./listuser.component.css']
})
export class ListuserComponent implements OnInit {

  private users:User[];
  private user:User;

  constructor(private userService:UserService, private router:Router) { }

  ngOnInit() {
    this.userService.getUsers().subscribe((users)=>{
      console.log(users);
      this.users = users;
    },(error)=>{
      console.log(error);
    })
  }

  deleteUser(user){
    this.userService.deleteUser(user.id).subscribe((data) =>{
      this.users.splice(this.users.indexOf(user),1);
    },(error)=>{
        console.log(error);
    });
  }

  updateUser(user){
    this.userService.setter(user);
    this.router.navigate(['/create']);
  }

  createUser(){
    let user = new User();
    this.userService.setter(user);
    this.router.navigate(['/create']);
  }

  searchUser(){
    var value = parseInt((<HTMLInputElement>document.getElementById("idInput")).value);
    this.userService.setID(value);
    // this.userService.getUser(value).subscribe((users)=>{
    //   console.log(users);
    //   this.user = users;
    // },(error)=>{
    //   console.log(error);
    // })
    // this.userService.setter(this.user);
    this.router.navigate(['/search']);
  }

}
