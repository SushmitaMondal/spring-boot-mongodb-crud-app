export class User {
  id: Number;
  name: string;
  designation: string;
  teamName: string;
}
