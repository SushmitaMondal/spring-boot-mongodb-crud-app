import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {User} from "../user";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class UserService {

  private user:User;
  private id:Number;

  private baseUrl:string='http://localhost:8080/app/users';
  constructor(private _http:HttpClient) { }

  public getUsers(): Observable<Array<User>>{
    return this._http.get<Array<User>>(this.baseUrl+"/all");
  }

  public getUser(id: Number): Observable<User>{
    return this._http.get<User>(this.baseUrl+"/"+id);
  }

  public deleteUser(id: Number): Observable<User>{
    return this._http.delete<User>(this.baseUrl+"/"+id);
  }

  public createUser(user: User): Observable<User>{
    return this._http.post<User>(this.baseUrl, user);
  }

  public updateUser(user: User): Observable<User>{
    return this._http.put<User>(this.baseUrl, user);
  }

  setter(user:User){
    this.user = user;
  }

  getter(){
    return this.user;
  }

  setID(id:Number){
    this.id = id;
  }

  getID(){
    return this.id;
  }
}
