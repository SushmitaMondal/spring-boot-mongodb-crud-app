package com.spring.example.springbootmongoDBexample.document;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Users {

    @Id
    private Integer id;
    private String name;
    private String designation;
    private String teamName;

    public Users( Integer id,String name, String designation, String teamName) {
        this.id = id;
        this.name = name;
        this.designation = designation;
        this.teamName = teamName;
   }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
