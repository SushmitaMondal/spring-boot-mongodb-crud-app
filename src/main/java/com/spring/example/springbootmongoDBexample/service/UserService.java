package com.spring.example.springbootmongoDBexample.service;

import com.spring.example.springbootmongoDBexample.document.Users;
import com.spring.example.springbootmongoDBexample.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UsersRepository anUsersrepository;

    public UserService(UsersRepository anUserRepository) {
        this.anUsersrepository = anUserRepository;
    }

    public List<Users> getAll(){
        return anUsersrepository.findAll();
    }

    public Optional<Users> getUser(Integer id){
        return anUsersrepository.findById(id);
    }

    public void insert(Users anUser){
        this.anUsersrepository.save(anUser);
    }

    public void update(Users anUser){
        this.anUsersrepository.save(anUser);
    }

    public void delete(Integer id){
        this.anUsersrepository.deleteById(id);
    }
}
