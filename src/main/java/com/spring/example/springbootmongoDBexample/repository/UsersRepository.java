package com.spring.example.springbootmongoDBexample.repository;

import com.spring.example.springbootmongoDBexample.document.Users;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UsersRepository extends MongoRepository <Users, Integer> {

}
