package com.spring.example.springbootmongoDBexample.controller;

import com.spring.example.springbootmongoDBexample.document.Users;
import com.spring.example.springbootmongoDBexample.repository.UsersRepository;
import com.spring.example.springbootmongoDBexample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping ("/app/users")
@CrossOrigin(origins="http://localhost:4200", allowedHeaders = "*")
public class UsersController {

    UserService anUsersService;

    @Autowired
    public UsersController(UserService anUsersService) {
        this.anUsersService = anUsersService;
    }

    @GetMapping("/all")
    public List<Users> getAll(){
        return anUsersService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Users> getUser(@PathVariable Integer id){
        return anUsersService.getUser(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void insert(@RequestBody Users anUser){
        this.anUsersService.insert(anUser);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody Users anUser){
        this.anUsersService.update(anUser);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id){
        this.anUsersService.delete(id);
    }
}
